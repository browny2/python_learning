# greeting = "Hello"
# name = input("Please enter your name ")
# print(greeting + ' ' + name)

splitString = "This string has been\nsplit over\nseveral\nlines"
print(splitString)

tabbedString ="1\t2\t3\t4\t5\t6\t7\t8\t9"
print(tabbedString)

print('The pet shop owner said "No,no,no, \'e\'s uh,...he\s resting"')
print("The pet shop owner said \"No,no,no, 'e's uh,...he\s resting\"\n")

anotherSplitString = """To make life easier python allows us 
to use triple quotes to 
allow code over multiple lines"""

print(anotherSplitString)