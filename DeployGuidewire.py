#!/usr/bin/python
__author__ = "Michael Brown"
import shutil
import os
import sys

"""This script has been created to assist in the build process for guidewire products"""

folderToRemove = ''
configSrc = ''
configDest = ''

# applicationName = ["ContactManager", "ClaimCenter", "PolicyCenter",
#                          "BillingCenter"]
applicationName = sys.argv[1:]
applicationInstallDir = '/tmp/'
configFolder = '/tmp/guidewire/'
configItems = ["Utilities", "Digital"] #add Digital back in
configGsrc = '/gsrc/itb'
configGtest = ["/gtest/itb", "/gtest/suite"]
configPcf = '/modules/configuration/config/web/pcf/suite'
configSharedItems = ["/gsrc", "/gtest"]
sharedCode = 'CommonConfiguration'

'''
Function created to complete folder clean up of the config files nested in the application (config)
As well as removing any Digital / Utilities files.
'''
def folderCleanup():
    print("")
    print("1. Check for which instance of guidewire is installed and remove the config folder")
    print("")
    for i in applicationName:
        if os.path.isdir(applicationInstallDir + i):
            if os.path.isdir(applicationInstallDir + i + "/modules/configuration"):
                print("Config folder for {0} exists removing...".format(i))
                folderToRemove = applicationInstallDir + i + "/modules/configuration"
                shutil.rmtree(folderToRemove)
            else:
                print("No config folders present for {0}".format(i))
            for j in configItems:
                if os.path.isdir(applicationInstallDir + i + "/" + j):
                    print("Config folder for {0}/{1} exists removing...".format(i, j))
                    folderToRemove = applicationInstallDir + i + "/" + j
                    shutil.rmtree(folderToRemove)
                else:
                    print("No config folders present for {0}/{1}".format(i, j))
        else:
            print("No config folders present for {0}".format(i))

'Function to copy directories'
def copyDirectory(src, dest):
    try:
        print("Copying contents of {0} to the {1} application folder".format(src, dest))
        shutil.copytree(src, dest)
    # Directories are the same
    except shutil.Error as e:
        print('Directory not copied. Error: %s' % e)
    # Any error saying that the directory doesn't exist
    except OSError as e:
        print('Directory not copied. Error: %s' % e)

'Function that copies the modules/configuration to the application'
def copyCoreConfigFiles():
    print("")
    print("2. Copying the modules/configuration folder to the application")
    print("")
    for i in applicationName:
        if os.path.isdir(applicationInstallDir + i):
            configSrc = configFolder + i + "/modules/configuration"
            configDest = applicationInstallDir + i + "/modules/configuration"
            copyDirectory(configSrc, configDest)
        else:
            print("No config folders present for {0}".format(i))

'Function that copies the Digital, Utilities, gsrc and gtest code to the application'
def copyDigitalUtilsFiles():
    print("")
    print("3. Copying the Digital and Utilities folders to the application")
    print("")
    for i in applicationName:
        if os.path.isdir(applicationInstallDir + i):
            for j in configItems:
                configSrc = configFolder + j
                configDest = applicationInstallDir + i + "/" + j
                copyDirectory(configSrc, configDest)
        else:
            print("No config folders present for {0}".format(i))

'Function to copy the gsrc configuration from the code base into the application directory'
def copyGsrcConfig():
    print("")
    print("4. Copying gsrc configuration from code base")
    print("")
    for i in applicationName:
        if os.path.isdir(applicationInstallDir + i + "/modules/configuration/"):
            configSrc = configFolder + sharedCode + configGsrc
            configDest = applicationInstallDir + i + "/modules/configuration" + configGsrc
            print(configSrc)
            print(configDest)
            copyDirectory(configSrc, configDest)
        else:
            print("No config folders present for {0}".format(i))

'Function to copy the gtest configuration from the code base into the application directory'
def copyGtestConfig():
    print("")
    print("5. Copying gtest configuration from code base")
    print("")
    for i in applicationName:
        if os.path.isdir(applicationInstallDir + i + "/modules/configuration/"):
            for j in configGtest:
                configSrc = configFolder + sharedCode + j
                configDest = applicationInstallDir + i + "/modules/configuration" + j
                copyDirectory(configSrc, configDest)
        else:
            print("No config folders present for {0}".format(i))

'Function to copy the PCF Suite configuration from the code base into the application directory'
def copyPcfConfig():
    print("")
    print("6. Copying pcf/suite configuration from code base")
    print("")
    for i in applicationName:
        if os.path.isdir(applicationInstallDir + i + "/modules/configuration/config/web/pcf"):
                configSrc = configFolder + sharedCode + "/config/web/pcf/suite"
                configDest = applicationInstallDir + i + configPcf
                copyDirectory(configSrc, configDest)
        else:
            print("No config folders present for {0}".format(i))

if len(applicationName) == 0:
    print ("Please enter one or more of the applications that you wish to deploy from the list ClaimCenter, BillingCenter, PolicyCenter or ContactManager i.e. DeployGuidewire.py ClaimCenter.")
else:
    # folderCleanup()
    # copyCoreConfigFiles()
    # copyDigitalUtilsFiles()
    # copyGsrcConfig()
    # copyGtestConfig()
    # copyPcfConfig()
    print ("Script is working")
