# shopping_list = ["milk", "pasta", "bread", "toast", "spam", "Yogurt", "rice"]
# for item in shopping_list:
#     if item == 'spam':
#         continue
#
#     print ("Buy " + item)
#
# print ("")
#
# shopping_list = ["milk", "pasta", "bread", "toast", "spam", "Yogurt", "rice"]
# for item in shopping_list:
#     if item == 'spam':
#         break
#
#     print ("Buy " + item)

meal = ["milk", "pasta", "bread", "toast", "spam", "Yogurt", "rice"]
nastyFoodItem = ''

for item in meal:
    if item == 'spam':
        nastyFoodItem = item
        break
else:
    print ("Give me some")

if nastyFoodItem:
    print("Can't I have anything without Spam!")


'''
Augmented Assignment
'''

number = "1,2,3,4,5,69,127,138,339"
cleanedNumber = ''

for i in range(0, len(number)):
    if number[i] in '0123456789':
        cleanedNumber += number[i]

newNumber = int(cleanedNumber)
print ("The number {0} is clean".format(cleanedNumber))


x = 23
x += 1
print (x)

x -= 4
print (x)

x *= 5
print (x)

x /= 4
print (x)

x **= 2
print (x)

x %= 60
print (x)


account = "Michael "
account += "Brown"
print (account)