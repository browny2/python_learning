greeting = "Bruce"
_myName = "Michael"
age = 26

print(age)

# print(_myName + age)

a = 12
b = 3

print(greeting + ' ' + _myName)
print(a + b)
print(a - b)
print(a * b)
print(a / b)
print(a // b)
print(a % b)

#Sums prioritise times and divide so the below shows as -35.0 not 12 as the one below it.
print(a + b / 3 - 4 * 12)
print((((a + b) / 3) - 4) * 12)

c = a + b
d = c / 3
e = d -4
print(e*12)

parrot = "Norwegian Blue"
print(parrot)
print(parrot[3])
print(parrot[-1])
print(parrot[0:6])
print(parrot[:6])
print(parrot[6:])
print(parrot[-4:-2])
#print characters from 0 - 6 but in steps of 2
print(parrot[0:6:2])
#print characters from 0 - 6 but in steps of 3
print(parrot[0:6:3])

#shows every 4th character from the 2nd
number ="9,223,372,036,854,75,807"
print  (number[1::4])

numbers = "1, 2, 3, 4, 5, 6, 7, 8, 9"
print (numbers[0::3])

string1 = "he's "
string2= "probably "
print (string1 + string2)
print ("He's " "probably " "pining")

print ("hello " *5)
print ("hello " * (5+4))
print ("hello " *5 + "4")

today = "friday"
print ("day" in today)
print ("fri" in today)
print ("parrot" in "fjord")
