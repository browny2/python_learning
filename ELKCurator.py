from elasticsearch import Elasticsearch
import curator

es = Elasticsearch([{'host': 'localhost'}])
def delete_indices(index, days):
    """ Delete indices older than X days """
    ilo = curator.IndexList(es)
    ilo.filter_by_regex(kind="prefix", value=index)
    ilo.filter_by_age(source="creation_date", direction="older", unit="days", unit_count=days)
    delete = curator.DeleteIndices(ilo)
    try:
        delete.do_action()
        print "Removed "+index+" indices older than "+days+" days old."
    except curator.NoIndices:
        print "No "+index+" available for removal."

delete_indices("netflow-index*", 7)
delete_indices("syslog-index*", 60)
